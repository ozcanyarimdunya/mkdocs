# Home 

This is home page for more click [me.](http://semiworld.org/)

## Introduction

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce egestas, quam molestie sollicitudin pellentesque, nunc nunc pharetra arcu, at pellentesque lacus quam quis libero. Sed tincidunt molestie arcu id tempor. Suspendisse potenti. Donec sed tellus rutrum, rutrum lectus in, commodo justo. Fusce nec mauris urna. Sed sodales tellus ac nisi volutpat, in auctor arcu dictum. Vivamus ligula risus, aliquet eget lacinia at, tincidunt a lacus. Sed dapibus orci sit amet ornare scelerisque. Pellentesque euismod quis mauris eget luctus. Morbi pretium finibus metus scelerisque vestibulum. Quisque lacus metus, luctus eget tortor vehicula, ullamcorper consectetur risus.

## Development 

Nullam dapibus bibendum vehicula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque convallis mi in urna interdum blandit. Aenean elementum mauris ac imperdiet dignissim. Nullam eget nibh cursus justo eleifend finibus. Aliquam leo nisi, varius non diam non, laoreet tempor purus. Sed quis tellus ut leo rhoncus tincidunt aliquet eu ex. Fusce ornare ornare massa, vel iaculis est accumsan non. Nulla non tincidunt quam.

```python
def say(message: str):
    print(f'Hello, {message}')
```

## Testing

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus ornare nec orci vel tincidunt. Aliquam et dapibus nulla. Vestibulum condimentum diam sed orci hendrerit porta. Maecenas ut posuere risus, vitae dictum odio. Phasellus dapibus purus erat, sed sollicitudin felis lobortis id. Sed luctus mattis arcu eu rhoncus. Cras malesuada et mi a laoreet.

## Deployment

Proin fermentum imperdiet nulla, sit amet mollis lacus ultricies quis.

### Local

 Maecenas vulputate mi quis convallis luctus. Sed at bibendum mi. Quisque at malesuada leo, vitae faucibus nunc. Sed augue eros, sollicitudin sed laoreet ut, iaculis ac felis. Nunc in convallis ipsum, quis lacinia risus. Mauris consequat maximus congue. Donec ac ultrices enim. Pellentesque ac semper lorem. Suspendisse pretium vulputate nisi, ac faucibus ipsum pharetra molestie. Vestibulum aliquet quis libero sed laoreet. 

### Remote 
Aliquam vulputate pulvinar dui. Ut commodo eros ac tellus cursus, in tempor libero convallis. Etiam sed odio nisi. Aenean non odio molestie, molestie nulla vel, blandit ante

## LICENSE

```
Phasellus libero augue, fermentum sed fringilla sodales, pulvinar at erat. 
Nullam nec eleifend elit. 
Proin lorem nibh, scelerisque quis sollicitudin vitae, ultricies eu risus. 
Nunc eleifend, est nec aliquam venenatis, mi felis vehicula mauris, quis varius ex quam quis risus. 
Nullam sodales vulputate ipsum, cursus tempor massa volutpat eu. 
Nunc tristique at tortor eget imperdiet. 
Etiam dui metus, aliquam sed lorem in, feugiat porttitor mi. 
Vestibulum blandit, eros non tincidunt tincidunt, libero justo dapibus massa, sed efficitur ligula ante non magna. 
Cras rutrum cursus dolor, quis porttitor dolor luctus pellentesque. 
Nullam vestibulum eget diam ut scelerisque. 
Mauris congue, velit id gravida gravida, sem nibh eleifend quam, placerat maximus nisi diam non libero. 
Quisque et neque volutpat massa porta pulvinar. 
In suscipit lacus ut lorem dapibus maximus. 
Pellentesque quis scelerisque leo. 
Nulla iaculis dolor urna. 
Nulla sodales risus ac lacus dapibus, sit amet imperdiet elit aliquet.
```
